<?php
class Api_model extends CI_Model
{
    public function getUser($uname = null, $pwd = null)
    {
        if ($uname == null && $pwd == null) {
            return $this->db->get('user')->result_array();
        } else {
            $this->db->where('username', $uname);
            $this->db->where('password', $pwd);
            return $this->db->get('user')->result_array();
        }
    }

    public function getMenu($nama = null, $jenis = null)
    {
        if ($nama == null && $jenis == null) {
            return $this->db->get('menu')->result_array();
        } else {
            $this->db->where('nama', $nama);
            $this->db->where('jenis_menu', $jenis);
            return $this->db->get('menu')->result_array();
        }
    }

    public function addMenu($data)
    {
        $this->db->insert('menu', $data);
        return $this->db->affected_rows();
    }

    public function updateMenu($data, $id)
    {
        $this->db->update('menu', $data, ['id_menu' => $id]);
        return $this->db->affected_rows();
    }

    public function deleteMenu($id)
    {
        $this->db->delete('menu', ['id_menu' => $id]);
        return $this->db->affected_rows();
    }
}

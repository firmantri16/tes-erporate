<?php
class Api extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Api_model', 'api');
    }

    public function login()
    {
        $username = $this->input->get('username');
        $password = $this->input->get('password');

        if (isset($username) && isset($username)) {
            $user = $this->api->getUser($username, $password);
        } else {
            $user = $this->api->getUser();
        }

        if ($user) {
            $data = [
                'status' => true,
                'message' => 'Data pengguna ditemukan',
                'data' => $user
            ];
            echo json_encode($data);
        } else {
            $data = [
                'status' => false,
                'message' => 'Data pengguna tidak ditemukan'
            ];
            echo json_encode($data);
        }
    }

    public function menu_get()
    {
        $nama = $this->input->get('nama');
        $jenis_menu = $this->input->get('jenis');

        if (isset($nama) && isset($jenis_menu)) {
            $menu = $this->api->getMenu($nama, $jenis_menu);
        } else {
            $menu = $this->api->getMenu();
        }

        if ($menu) {
            $data = [
                'status' => true,
                'message' => 'Data menu ditemukan.',
                'data' => $menu
            ];
            echo json_encode($data);
        } else {
            $data = [
                'status' => false,
                'message' => 'Data menu tidak ditemukan.'
            ];
            echo json_encode($data);
        }
    }

    public function menu_post()
    {
        $nama = $this->input->post('nama');
        $jenis_menu = $this->input->post('jenis');
        $harga = $this->input->post('harga');
        $status = $this->input->post('status');

        if (isset($nama) && isset($jenis_menu) && isset($harga) && isset($status)) {
            $data_menu = [
                'nama' => $nama,
                'jenis_menu' => $jenis_menu,
                'harga' => $harga,
                'status' => $status
            ];

            if ($this->api->addMenu($data_menu) > 0) {
                $data = [
                    'status' => true,
                    'message' => 'Berhasil menambahkan menu baru.'
                ];
                echo json_encode($data);
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Gagal menambahkan menu baru.'
                ];
                echo json_encode($data);
            }
        } else {
            $data = [
                'status' => false,
                'message' => 'Sediakan nama, jenis, harga, dan status.'
            ];
            echo json_encode($data);
        }
    }

    public function menu_put()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $jenis_menu = $this->input->post('jenis');
        $harga = $this->input->post('harga');
        $status = $this->input->post('status');

        if (isset($id) && isset($nama) && isset($jenis_menu) && isset($harga) && isset($status)) {
            $data_menu = [
                'nama' => $nama,
                'jenis_menu' => $jenis_menu,
                'harga' => $harga,
                'status' => $status
            ];
            if ($this->api->updateMenu($data_menu, $id) > 0) {
                $data = [
                    'status' => true,
                    'message' => 'Berhasil mengubah menu.'
                ];
                echo json_encode($data);
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Gagal mengubah menu.'
                ];
                echo json_encode($data);
            }
        } else {
            $data = [
                'status' => false,
                'message' => 'Sediakan id, nama, jenis, harga, dan status.'
            ];
            echo json_encode($data);
        }
    }

    public function menu_delete()
    {
        $id = $this->input->post('id');

        if (isset($id)) {
            if ($this->api->deleteMenu($id) > 0) {
                $data = [
                    'status' => true,
                    'message' => 'Berhasil menghapus menu.'
                ];
                echo json_encode($data);
            } else {
                $data = [
                    'status' => false,
                    'message' => 'Gagal menghapus menu.'
                ];
                echo json_encode($data);
            }
        } else {
            $data = [
                'status' => false,
                'message' => 'Sediakan id untuk menghapus menu.'
            ];
            echo json_encode($data);
        }
    }
}

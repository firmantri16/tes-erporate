<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">Code Pemesanan</th>
			<th scope="col">No. Meja</th>
			<th scope="col">Tanggal</th>
			<th scope="col">Total Harga</th>
			<th scope="col">Status Pembayaran</th>
		</tr>
	</thead>
	<tbody>
		<?php if (count($pembayaran) == 0) { ?>
		<tr>
			<td colspan="6"><strong>Tidak ada pembayaran yang aktif</strong></td>
		</tr>
		<?php } else { ?>
		<?php for ($i = 0; $i < count($pembayaran); $i++) : ?>
		<tr>
			<th scope="row"><?= $i + 1 ?></th>
			<td id="idPesan<?= $i + 1 ?>"><?= $pembayaran[$i]['id_pemesanan'] ?></td>
			<td><?= $pembayaran[$i]['no_meja'] ?></td>
			<td><?= $pembayaran[$i]['tgl_pesan'] ?></td>
			<td><?= $pembayaran[$i]['total_harga'] ?></td>
			<td>
				<div class="row">
					<div class="col-sm-8">
						<?php if ($pembayaran[$i]['status'] == 'Belum Dibayar') { ?>
						<select class="custom-select custom-select-sm" name="status_bayar" id="kode_status<?= $i + 1 ?>" onchange="updatePembayaran(document.getElementById('idPesan<?= $i + 1 ?>').innerHTML, <?= $i + 1 ?>)">
							<option value="Belum Dibayar" selected>Belum Dibayar</option>
							<option value="Sudah Dibayar">Sudah Dibayar</option>
						</select>
						<?php } else { ?>
						<select class="custom-select custom-select-sm" name="status_bayar" id="kode_status<?= $i + 1 ?>" onchange="updatePembayaran(document.getElementById('idPesan<?= $i + 1 ?>').innerHTML, <?= $i + 1 ?>)" disabled>
							<option value="Belum Dibayar">Belum Dibayar</option>
							<option value="Sudah Dibayar" selected>Sudah Dibayar</option>
						</select>
						<?php } ?>
					</div>
				</div>
			</td>
		</tr>
		<?php endfor ?>
		<?php } ?>
	</tbody>
</table>
<script>
	function updatePembayaran(kodePemesanan, kodeStatus) {
		$.ajax({
			type: 'GET',
			url: '<?= base_url('pembayaran/bayar/') ?>' + kodePemesanan,
			success: function(ret) {
				window.alert('Pesanan ' + kodePemesanan + ' sudah dibayar');
			}
		});
		$('#kode_status' + kodeStatus).attr('disabled', true);
	}
</script>
<?php 
class Pembayaran_model extends CI_Model{
    public function allOrder(){
        $this->db->select('pemesanan.*, pembayaran.id_bayar, pembayaran.status, pembayaran.total_harga');
        $this->db->join('pembayaran', 'pembayaran.id_pemesanan = pemesanan.id_pemesanan');
        return $this->db->get('pemesanan')->result_array();
    }

    public function updatePembayaran($tbl, $status, $kode){
        $this->db->set('status', $status[$tbl]);
        $this->db->where('id_pemesanan', $kode);
        $this->db->update($tbl);
    }
}
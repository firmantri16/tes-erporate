<?php 
class Pembayaran extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Pembayaran_model', 'pembayaran');
    }

    public function index(){
        if($this->session->userdata('username') != ''){
            if($this->session->userdata('jabatan') == 'Kasir'){
                $data['pembayaran'] = $this->pembayaran->allOrder();
    
                $this->load->view('template/header');
                $this->load->view('pembayaran/index', $data);
                $this->load->view('template/footer');
            }else{
                redirect(base_url('home'));
            }
        }else{
            $this->load->view('auth/index');
        }
    }

    public function bayar($kode){
        $status = [
            'pembayaran' => 'Sudah Dibayar',
            'pemesanan' => 'Tidak Aktif'
        ];
        $this->pembayaran->updatePembayaran('pembayaran', $status, $kode);
        $this->pembayaran->updatePembayaran('pemesanan', $status, $kode);
        echo "Pesanan telah dibayar - ".$kode;
    }
}
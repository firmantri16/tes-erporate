<?php 
class Pemesanan_model extends CI_Model{
    private $table = 'pemesanan';

    public function getMenu($params){
        $this->db->where('jenis_menu', $params);
        return $this->db->get('menu')->result_array();
    }

    public function getData($params){
        $this->db->select('id_menu, harga');
        $this->db->where('nama', $params[0]);
        $this->db->where('jenis_menu', $params[1]);
        return $this->db->get('menu')->result_array();
    }

    public function countRecord(){
        return $this->db->count_all($this->table);
    }

    public function add($tbl, $params){
        $this->db->insert($tbl, $params);
    }
    
    public function addDetailPesan($idPesan, $idMenu){
        $data=[
            'id_pemesanan' => $idPesan,
            'id_menu' => $idMenu,
        ];
        $this->db->insert('detail_pemesanan', $data);
    }

    public function allOrder(){
        $this->db->select('pemesanan.*, pembayaran.id_bayar, pembayaran.total_harga');
        $this->db->where('pemesanan.status', 'Aktif');
        $this->db->join('pembayaran', 'pembayaran.id_pemesanan = pemesanan.id_pemesanan');
        return $this->db->get('pemesanan')->result_array();
    }

    public function delete($tbl, $id){
        $this->db->where('id_pemesanan', $id);
        $this->db->delete($tbl);
    }

    public function getOrderById($id){
        $this->db->select('ps.*, m.id_menu, m.nama, m.jenis_menu, m.harga');
        $this->db->where('ps.id_pemesanan', $id);
        $this->db->join('detail_pemesanan as d', 'd.id_pemesanan = ps.id_pemesanan');
        $this->db->join('menu as m', 'm.id_menu = d.id_menu');
        return $this->db->get('pemesanan as ps')->result_array();
    }

    public function hapusMenu($kode, $id){
        $this->db->delete('detail_pemesanan', ['id_pemesanan'=>$kode, 'id_menu'=>$id]);
    }
}
<h4 class="mb-4 text-gray-800">Tambah Pemesanan</h4>
<form method="POST" action="<?= base_url('pemesanan/tambah')?>">
	<div class="form-group row">
		<label for="inputEmail3" class="col-sm-2 col-form-label">No. Meja</label>
		<div class="col-sm-2">
			<input type="number" name="no-meja" class="form-control" id="inputEmail3" placeholder="No. Meja">
		</div>
	</div>
	<fieldset class="form-group">
		<div class="row">
			<div class="col-sm-6">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Makanan</th>
							<th scope="col">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php for($i = 0; $i < count($makanan); $i++): ?>
						<tr>
							<td>
								<div class="custom-control custom-checkbox">
									<?php if($makanan[$i]['status'] == 'Ready'){?>
									<input type="checkbox" name="makanan[]" class="custom-control-input"
										id="makanan<?= $i+1?>" value="<?= $makanan[$i]['nama']?>">
									<label class="custom-control-label"
										for="makanan<?= $i+1?>"><?= $makanan[$i]['nama']?></label>
									<?php }else{?>
									<input type="checkbox" name="makanan[]" class="custom-control-input"
										id="makanan<?= $i+1?>" value="<?= $makanan[$i]['nama']?>" disabled>
									<label class="custom-control-label"
										for="makanan<?= $i+1?>"><?= $makanan[$i]['nama']?></label>
									<?php }?>
								</div>
							</td>
							<td>
								<?php if($makanan[$i]['status'] == 'Ready'){?>
								<span class="badge badge-success"><?= $makanan[$i]['status']?></span>
								<?php }else{?>
								<span class="badge badge-danger"><?= $makanan[$i]['status']?></span>
								<?php }?>
							</td>
						</tr>
						<?php endfor?>
					</tbody>
				</table>
			</div>
		</div>
	</fieldset>
	<fieldset class="form-group">
		<div class="row">
			<div class="col-sm-6">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Minuman</th>
							<th scope="col">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php for($i = 0; $i < count($minuman); $i++): ?>
						<tr>
							<td>
								<div class="custom-control custom-checkbox">
									<?php if($minuman[$i]['status'] == 'Ready'){?>
									<input type="checkbox" name="minuman[]" class="custom-control-input"
										id="minuman<?= $i+1?>" value="<?= $minuman[$i]['nama']?>">
									<label class="custom-control-label"
										for="minuman<?= $i+1?>"><?= $minuman[$i]['nama']?></label>
									<?php }else{?>
									<input type="checkbox" name="minuman[]" class="custom-control-input"
										id="minuman<?= $i+1?>" value="<?= $minuman[$i]['nama']?>" disabled>
									<label class="custom-control-label"
										for="minuman<?= $i+1?>"><?= $minuman[$i]['nama']?></label>
									<?php }?>
								</div>
							</td>
							<td>
								<?php if($minuman[$i]['status'] == 'Ready'){?>
								<span class="badge badge-success"><?= $minuman[$i]['status']?></span>
								<?php }else{?>
								<span class="badge badge-danger"><?= $minuman[$i]['status']?></span>
								<?php }?>
							</td>
						</tr>
						<?php endfor?>
					</tbody>
				</table>
			</div>
		</div>
	</fieldset>
	<div class="form-group row">
		<div class="col-sm-10">
			<button type="submit" class="btn btn-primary">Simpan Pesanan</button>
		</div>
	</div>
</form>

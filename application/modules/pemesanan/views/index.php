<a class="btn btn-primary" href="<?= base_url('pemesanan/tambah')?>" role="button"><i class="fas fa-plus"></i> Tambah
	Pesanan</a>
<div class="daftar-pesanan">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Code Pemesanan</th>
				<th scope="col">No. Meja</th>
				<th scope="col">Tanggal</th>
				<th scope="col">Total Harga</th>
				<th scope="col">Status</th>
				<th scope="col">Aksi</th>
			</tr>
		</thead>
		<tbody id="data-pesanan">
			<?php if(count($pesanan) == 0){?>
			<tr>
				<td colspan="6"><strong>Tidak ada pesanan yang aktif</strong></td>
			</tr>
			<?php }else{?>
			<?php for ($i=0; $i < count($pesanan); $i++):?>
			<tr>
				<th scope="row"><?= $i+1?></th>
				<td id="pesanan<?= $i+1?>"><?= $pesanan[$i]['id_pemesanan']?></td>
				<td><?= $pesanan[$i]['no_meja']?></td>
				<td><?= $pesanan[$i]['tgl_pesan']?></td>
				<td><?= $pesanan[$i]['total_harga']?></td>
				<td><?= $pesanan[$i]['status']?></td>
				<td>
					<a href="<?= base_url('pemesanan/detail/'.$pesanan[$i]['id_pemesanan'])?>"><span
							class="btn btn-primary btn-sm"><i class="fas fa-search"></i></span></a>
					<a href="<?= base_url('pemesanan/delete/'.$pesanan[$i]['id_pemesanan'])?>"><span
					class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></span></a>
				</td>
			</tr>
			<?php endfor?>
			<?php }?>
		</tbody>
	</table>
</div>
<?php 
class Pemesanan extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Pemesanan_model', 'pemesanan');
    }
    
    public function index(){
        if($this->session->userdata('username') != ''){
            $data['pesanan'] = $this->pemesanan->allOrder();

            $this->load->view('template/header');
            $this->load->view('pemesanan/index', $data);
            $this->load->view('template/footer');
        }else{
            $this->load->view('auth/index');
        }
    }

    public function tambah(){
        $noMeja = $this->input->post('no-meja');
        $makanan = $this->input->post('makanan');
        $minuman = $this->input->post('minuman');

        if(isset($noMeja) && isset($makanan) && isset($minuman)){
            $idMakanan = [];
            $idMakanan = [];
            $hargaTotal = 0;
            $jmlRecord = $this->pemesanan->countRecord();
            $codePemesanan = 'ERP'.date('dmY').'-'.($jmlRecord+1);

            for ($i=0; $i < count($makanan); $i++) { 
                $result = $this->pemesanan->getData([$makanan[$i], 'makanan']);
                $idMakanan[$i] = $result[0]['id_menu'];
                $hargaTotal += $result[0]['harga'];
            }

            for ($i=0; $i < count($minuman); $i++) { 
                $result = $this->pemesanan->getData([$minuman[$i], 'minuman']);
                $idMinuman[$i] = $result[0]['id_menu'];
                $hargaTotal += $result[0]['harga'];
            }

            $dataPemesanan = [
                'id_pemesanan' => $codePemesanan,
                'no_meja' => $noMeja,
                'tgl_pesan' => date('Y-m-d'),
                'status' => 'Aktif',
                'username' => $this->session->userdata('username')
            ];
            $this->pemesanan->add('pemesanan', $dataPemesanan);
            for ($i=0; $i < count($idMakanan); $i++) { 
                $this->pemesanan->addDetailPesan($codePemesanan, $idMakanan[$i]);
            }

            for ($i=0; $i < count($idMinuman); $i++) { 
                $this->pemesanan->addDetailPesan($codePemesanan, $idMinuman[$i]);
            }
            $dataPembayaran = [
                'total_harga' => $hargaTotal,
                'status' => 'Belum Dibayar',
                'id_pemesanan' => $codePemesanan
            ];
            $this->pemesanan->add('pembayaran', $dataPembayaran);
            
            redirect(base_url('pemesanan'));
        }else{
            $data['makanan'] = $this->pemesanan->getMenu('makanan');
            $data['minuman'] = $this->pemesanan->getMenu('minuman');

            $this->load->view('template/header');
            $this->load->view('pemesanan/tambah', $data);
            $this->load->view('template/footer');
        }
    }

    public function delete($id){
        if($this->session->userdata('username') != ''){
            //hapus detail, pembayaran, pemesanan
            $table = ['detail_pemesanan', 'pembayaran', 'pemesanan'];
            $this->pemesanan->delete($table, $id);

            redirect(base_url('pemesanan'));
        }else{
            $this->load->view('auth/index');
        }
    }

    public function detail($id){
        if($this->session->userdata('username') != ''){
            $data['id'] = $id;
            $this->load->view('template/header');
            $this->load->view('pemesanan/detail', $data);
            $this->load->view('template/footer');
        }else{
            $this->load->view('auth/index');
        }
    }

    public function load($id){
        $data = $this->pemesanan->getOrderById($id);

        $output = '<a class="btn btn-primary" href="'.base_url("pemesanan/tambah").'" role="button"><i class="fas fa-plus"></i> Tambah
        Menu Pesanan Ini</a>
        <div style="margin-top: 20px;">
        <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode Pemesanan</th>
                                <th scope="col">No. Meja</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Status</th>
                                <th scope="col">Nama Menu</th>
                                <th scope="col">Jenis Menu</th>
                                <th scope="col">Harga</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>';
        for ($i=0; $i < count($data); $i++) { 
            $angka = $i+1;
            $output .= '<tr>
                            <th scope="row">'.$angka.'</th>
                            <td>'.$data[$i]['id_pemesanan'].'</td>
                            <td>'.$data[$i]['no_meja'].'</td>
                            <td>'.$data[$i]['tgl_pesan'].'</td>
                            <td>'.$data[$i]['status'].'</td>
                            <td>'.$data[$i]['nama'].'</td>
                            <td>'.$data[$i]['jenis_menu'].'</td>
                            <td>'.$data[$i]['harga'].'</td>
                            <td><button type="button" id="hapusMenu" onclick="hapusMenu(\''.$id.'\', '.$data[$i]["id_menu"].')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button></td>
                        </tr>';
        }
        $output .= '</tbody>
                    </table>
                    </div>';
        echo $output;
    }

    public function menu_hapus($kode, $id){
        $this->pemesanan->hapusMenu($kode, $id);
        echo "Berhasil menghapus menu";
    }
}
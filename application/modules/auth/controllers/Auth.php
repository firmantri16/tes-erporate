<?php 
class Auth extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Auth_model', 'auth');
    }
    
    public function index(){
        if($this->session->userdata('username') != ''){
            redirect(base_url('home'));
        }else{
            $this->load->view('auth/index');
        }
    }

    public function login(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $data = $this->auth->verifikasi($username, $password);

        // $this->pre($data);
        if($data[0]['nama'] != ''){
            $session_data = array(
                'username' => $data[0]['username'],
                'nama' => $data[0]['nama'],
                'jabatan' => $data[0]['jabatan']
            );
            $this->session->set_userdata($session_data);
            
            redirect(base_url('home'));
        }else{
            redirect(base_url('auth'));
        }
    }

    public function logout(){
        if($this->session->userdata('username') != ''){
            $this->session->unset_userdata('username');
            $this->session->unset_userdata('nama');
            redirect(base_url("auth"));
        }
    }
}
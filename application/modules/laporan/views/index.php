<label class="h4 cetak-laporan">Laporan : <?=$this->session->userdata('nama');?>- <?=$this->session->userdata('jabatan');?></label>
<a class="btn btn-primary" href="<?= base_url('laporan/cetak')?>" role="button"
	style="float: right; margin-bottom: 20px;"><i class="fas fa-print"></i> Cetak Laporan</a>
<div class="laporan">
	<table class="table table-hover">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Kode Pemesanan</th>
				<th scope="col">No. Meja</th>
				<th scope="col">Tanggal</th>
				<th scope="col">Status Pemesanan</th>
				<th scope="col">Total harga</th>
			</tr>
		</thead>
		<tbody>
            <?php if(count($laporan) == 0){?>
            <tr>
				<td colspan="6"><strong>Tidak ada laporan</strong></td>
			</tr>
            <?php }else{ ?>
                <?php for ($i=0; $i < count($laporan); $i++): ?>
                <tr>
                    <th scope="row"><?=$i+1?></th>
                    <td><?=$laporan[$i]['id_pemesanan']?></td>
                    <td><?=$laporan[$i]['no_meja']?></td>
                    <td><?=$laporan[$i]['tgl_pesan']?></td>
                    <td><?=$laporan[$i]['status']?></td>
                    <td><?=$laporan[$i]['total_harga']?></td>
                </tr>
                <?php endfor?>
            <?php }?>
            </tbody>
	</table>
</div>

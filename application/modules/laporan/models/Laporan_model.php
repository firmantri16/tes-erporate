<?php 
class Laporan_model extends CI_Model{
    public function getLaporan($user)
    {
        $this->db->select('ps.*, pb.total_harga, u.nama, u.jabatan');
        $this->db->join('pembayaran as pb', 'pb.id_pemesanan = ps.id_pemesanan');
        $this->db->join('user as u', 'u.username = ps.username');
        $this->db->where('u.username', $user);
        $this->db->where('ps.status', 'Tidak Aktif');
        return $this->db->get('pemesanan as ps')->result_array();
    }
}
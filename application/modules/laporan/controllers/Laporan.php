<?php 

require_once('assets/print/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Laporan extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Laporan_model', 'laporan');
    }

    public function index(){
        if($this->session->userdata('username') != ''){
            $user = $this->session->userdata('username');
            $data['laporan'] = $this->laporan->getLaporan($user);

            $this->load->view('template/header');
            $this->load->view('laporan/index', $data);
            $this->load->view('template/footer');
            // echo "<pre>";
            // print_r($data['laporan']);
            // echo "</pre>";
        }else{
            $this->load->view('auth/index');
        }
    }

    public function cetak(){
        if($this->session->userdata('username') != ''){

            $user = $this->session->userdata('username');
            $nama = $this->session->userdata('nama');
            $data = $this->laporan->getLaporan($user);

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1','No.');
            $sheet->setCellValue('B1','Kode Pemesanan');
            $sheet->setCellValue('C1','No. Meja');
            $sheet->setCellValue('D1','Tanggal');
            $sheet->setCellValue('E1','Status Pemesanan');
            $sheet->setCellValue('F1','Total Harga');

            for ($index=0; $index < count($data); $index++) {
                $cell = $index+2;
                $sheet->setCellValue('A'.$cell, $index+1);
                $sheet->setCellValue('B'.$cell, $data[$index]['id_pemesanan']);
                $sheet->setCellValue('C'.$cell, $data[$index]['no_meja']);
                $sheet->setCellValue('D'.$cell, $data[$index]['tgl_pesan']);
                $sheet->setCellValue('E'.$cell, $data[$index]['status']);
                $sheet->setCellValue('F'.$cell, $data[$index]['total_harga']);
            }

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="Laporan ('.$nama.' '.date('d-m-Y').').xlsx"');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');
        }
    }
}
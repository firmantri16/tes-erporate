<?php 
class Home extends MY_Controller{
    function __construct(){
        parent::__construct();
    }
    
    public function index(){
        if($this->session->userdata('username') != ''){

            $this->load->view('template/header');
            $this->load->view('home/index');
            $this->load->view('template/footer');
        }else{
            redirect(base_url('auth'));
        }
    }
}